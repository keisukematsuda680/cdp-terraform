resource "aws_efs_file_system" "tf-efs" {
  tags = {
    Name = "tf-efs"
  }
}

resource "aws_efs_file_system" "tf-efs-lifecycle-policy" {
  lifecycle_policy {
    transition_to_ia = "AFTER_30_DAYS"
  }
}

resource "aws_efs_mount_target" "tf-efs-mount-target-private-a" {
  file_system_id  = aws_efs_file_system.tf-efs.id
  subnet_id       = aws_subnet.public-a.id
  security_groups = ["${module.security_group_efs.this_security_group_id}"]
}

resource "aws_efs_mount_target" "tf-efs-mount-target-private-c" {
  file_system_id  = aws_efs_file_system.tf-efs.id
  subnet_id       = aws_subnet.public-c.id
  security_groups = ["${module.security_group_efs.this_security_group_id}"]
}
