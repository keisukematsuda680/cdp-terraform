# VPC
resource "aws_vpc" "terraform" {
  cidr_block           = "10.1.0.0/16"
  enable_dns_support   = true
  enable_dns_hostnames = true

  tags = {
    Name = "terraform"
  }
}

# Subnet
resource "aws_subnet" "public-a" {
  vpc_id                  = aws_vpc.terraform.id
  cidr_block              = "10.1.1.0/24"
  map_public_ip_on_launch = true
  availability_zone       = "ap-northeast-1a"
  tags = {
    Name = "public-a"
  }
}

resource "aws_subnet" "public-c" {
  vpc_id                  = aws_vpc.terraform.id
  cidr_block              = "10.1.2.0/24"
  map_public_ip_on_launch = true
  availability_zone       = "ap-northeast-1c"
  tags = {
    Name = "public-c"
  }
}

resource "aws_subnet" "private-a" {
  vpc_id                  = aws_vpc.terraform.id
  cidr_block              = "10.1.11.0/24"
  map_public_ip_on_launch = false
  availability_zone       = "ap-northeast-1a"
  tags = {
    Name = "private-a"
  }
}

resource "aws_subnet" "private-c" {
  vpc_id                  = aws_vpc.terraform.id
  cidr_block              = "10.1.12.0/24"
  map_public_ip_on_launch = false
  availability_zone       = "ap-northeast-1c"
  tags = {
    Name = "private-c"
  }
}

# Internet Gateway
resource "aws_internet_gateway" "igw" {
  vpc_id = aws_vpc.terraform.id
  tags = {
    Name = "private"
  }
}

# Route Table
resource "aws_route_table" "public" {
  vpc_id = aws_vpc.terraform.id
  tags = {
    Name = "public"
  }
}

resource "aws_route" "public" {
  route_table_id         = aws_route_table.public.id
  gateway_id             = aws_internet_gateway.igw.id
  destination_cidr_block = "0.0.0.0/0"
}

resource "aws_route_table_association" "public-a" {
  subnet_id      = aws_subnet.public-a.id
  route_table_id = aws_route_table.public.id
}

resource "aws_route_table_association" "public-c" {
  subnet_id      = aws_subnet.public-c.id
  route_table_id = aws_route_table.public.id
}

resource "aws_route_table" "private" {
  vpc_id = aws_vpc.terraform.id
}

resource "aws_route_table_association" "private-a" {
  subnet_id      = aws_subnet.private-a.id
  route_table_id = aws_route_table.private.id
}

resource "aws_route_table_association" "private-c" {
  subnet_id      = aws_subnet.private-c.id
  route_table_id = aws_route_table.private.id
}


# Security Group
resource "aws_security_group" "sg-db" {
  name   = "sgdb"
  vpc_id = aws_vpc.terraform.id
  tags = {
    Name = "sg-db"
  }
}

resource "aws_security_group" "sg-ec2" {
  name   = "sgec2"
  vpc_id = aws_vpc.terraform.id
  tags = {
    Name = "sg-ec2"
  }
}

# Security Group
module "security_group_ec2" {
  source      = "terraform-aws-modules/security-group/aws"
  name        = "public-alb-sg"
  description = "Security Group for ec2"
  vpc_id      = aws_vpc.terraform.id

  ingress_cidr_blocks = ["0.0.0.0/0"]
  ingress_rules       = ["ssh-tcp", "https-443-tcp", "http-80-tcp"]
  egress_rules        = ["all-all"]
}

module "security_group_rds" {
  source      = "terraform-aws-modules/security-group/aws"
  name        = "public-rds-sg"
  description = "Security Group for rds"
  vpc_id      = aws_vpc.terraform.id

  ingress_cidr_blocks = ["0.0.0.0/0"]
  ingress_rules       = ["mysql-tcp"]
  egress_rules        = ["all-all"]
}


module "security_group_efs" {
  source      = "terraform-aws-modules/security-group/aws"
  name        = "public-efs-sg"
  description = "Security Group for efs"
  vpc_id      = aws_vpc.terraform.id

  ingress_cidr_blocks = ["0.0.0.0/0"]
  ingress_rules       = ["nfs-tcp"]
  egress_rules        = ["all-all"]
}

module "security_group_alb" {
  source      = "terraform-aws-modules/security-group/aws"
  name        = "public-alb-sg"
  description = "Security Group for ALB"
  vpc_id      = aws_vpc.terraform.id

  ingress_cidr_blocks = ["0.0.0.0/0"]
  ingress_rules       = ["https-443-tcp", "http-80-tcp"]
  egress_rules        = ["all-all"]
}
