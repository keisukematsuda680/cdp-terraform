locals {
  storage_type   = "gp2"
  engine         = "mysql"
  engine_version = "8.0.20"
  instance_class = "db.t2.micro"
  database_name  = "wordpress"
}

resource "aws_db_subnet_group" "tf-db-subnet" {
  name       = "tf-db-subnet"
  subnet_ids = ["${aws_subnet.private-a.id}", "${aws_subnet.private-c.id}"]
  tags = {
    Name = "tf-db-subnet"
  }
}

resource "aws_db_instance" "tf-db" {
  identifier             = "tf-db"
  allocated_storage      = 30
  storage_type           = local.storage_type
  engine                 = local.engine
  engine_version         = local.engine_version
  instance_class         = local.instance_class
  name                   = "wordpress"
  username               = "wordpress"
  password               = "wordpresspasswd"
  vpc_security_group_ids = ["${module.security_group_rds.this_security_group_id}"]
  db_subnet_group_name   = aws_db_subnet_group.tf-db-subnet.id
  skip_final_snapshot    = true
  # snapshot_identifier = 
  backup_retention_period = 1
}

# resource "aws_db_instance" "tf-db" {
#   identifier = "tf-db"
#   name = "wordpress"
#   vpc_security_group_ids = ["${module.security_group_rds.this_security_group_id}"]
#   db_subnet_group_name   = aws_db_subnet_group.tf-db-subnet.id
#   snapshot_identifier = data.aws_db_snapshot.latest-snapshot.id
# }

# なぜかうまくいかない。。。
# resource "aws_db_instance" "tf-db-replica" {
#   identifier              = "tf-db-replica"
#   allocated_storage       = 30
#   storage_type            = local.storage_type
#   engine                  = local.engine
#   engine_version          = local.engine_version
#   instance_class          = local.instance_class
#   name                    = "initial_db"
#   username                = "admin"
#   password                = "password"
#   vpc_security_group_ids  = ["${aws_security_group.sg-db.id}"]
#   db_subnet_group_name    = aws_db_subnet_group.tf-db-subnet.id
#   skip_final_snapshot     = true
#   replicate_source_db     = aws_db_instance.tf-db.id
#   backup_retention_period = 1
# }

# provider "mysql" {
#   endpoint = aws_db_instance.tf-db.endpoint
#   username = aws_db_instance.tf-db.username
#   password = aws_db_instance.tf-db.password
# }

# resource "mysql_user" "user-wordpress" {
#   user               = "wordpress"
#   host               = "%"
#   plaintext_password = "wordpresspasswd"
# }

# resource "mysql_database" "database-wordpress" {
#   name = local.database_name
# }

# resource "mysql_grant" "grant-wordpress" {
#   user       = mysql_user.user
#   host       = mysql_user.host
#   database   = mysql_database.name
#   privileges = ["ALL"]
# }

data "aws_db_snapshot" "snapshot" {
  db_instance_identifier = aws_db_instance.tf-db.id
  most_recent            = true
}
