# ELB(ALB)
resource "aws_lb" "tf-alb" {
  name               = "tf-alb"
  internal           = false
  load_balancer_type = "application"
  subnets            = ["${aws_subnet.public-a.id}", "${aws_subnet.public-c.id}"]
  security_groups    = [module.security_group_alb.this_security_group_id]
}

resource "aws_lb_listener" "tf-alb-listener" {
  load_balancer_arn = aws_lb.tf-alb.arn
  port              = 80
  protocol          = "HTTP"

  default_action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.tf-alb-target-group.arn
  }
}

resource "aws_lb_target_group" "tf-alb-target-group" {
  name     = "tf-alb-target-group"
  port     = 80
  protocol = "HTTP"
  vpc_id   = aws_vpc.terraform.id

  health_check {
    healthy_threshold   = 2
    unhealthy_threshold = 2
    timeout             = 3
    interval            = 30
    protocol            = "HTTP"
    path                = "/"
  }
}
