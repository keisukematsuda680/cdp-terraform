locals {
  iam_policy_file = "ec2_for_s3.json"
}

# EC2 Key Pairs
resource "aws_key_pair" "terraform-kp" {
  key_name   = "terraform-kp"
  public_key = file(var.public_key_path)
}

# EC2 IAM
module "iam_role_for_ec2" {
  source    = "./modules/iam"
  name      = "iam_for_ec2"
  identifer = "ec2.amazonaws.com"
  policy    = file("./modules/iam/policies/${local.iam_policy_file}")
}

resource "aws_iam_instance_profile" "instance_profile_for_ec2" {
  name = "instance_profile_for_ec2"
  role = module.iam_role_for_ec2.iam_role_name
}

# EC2 Instance
resource "aws_instance" "tf-instance" {
  ami                    = "ami-0138ef0d838de5741"
  instance_type          = "t2.micro"
  key_name               = aws_key_pair.terraform-kp.key_name
  vpc_security_group_ids = ["${module.security_group_ec2.this_security_group_id}"]
  subnet_id              = aws_subnet.public-a.id
  iam_instance_profile   = aws_iam_instance_profile.instance_profile_for_ec2.name
  user_data              = <<EOF
    #!/bin/bash
    EFS_MOUNT_TARGET="${aws_efs_mount_target.tf-efs-mount-target-private-a.dns_name}:/"
    EFS_MOUNT_POINT="/var/www/html"
    WP_CONFIG_FILE_PATH="/var/www/html/wp-config.php"
    MYSQL_DUMP_BACKET_NAME="s3://terraform-backet-2021"
    MYSQL_DUMP_NAME="wordpress.dump"
    MYSQL_ENDPOINT="${aws_db_instance.tf-db.endpoint}"


    echo "$${EFS_MOUNT_TARGET} $${EFS_MOUNT_POINT} efs _netdev,tls 0 0" | tee -a /etc/fstab
    mount -a
    chown apache:apache /var/www/html
    cp -nRp /var/www/html-old/* /var/www/html/
    aws s3 cp $MYSQL_DUMP_BACKET_NAME/$MYSQL_DUMP_NAME /tmp/
    sed -ie "s/db.clouddesignpattern.club/$MYSQL_ENDPOINT/g" $WP_CONFIG_FILE_PATH
    systemctl start httpd
  EOF
}

# Create AMI from EC2 Instance
resource "aws_ami_from_instance" "tf-ami-from-ec2" {
  name               = "tf-ami-from-ec2"
  source_instance_id = aws_instance.tf-instance.id
  
}

# Delete EC2 Instance after creating AMI
# resource "null_resource" "delete-ec2-after-creating-ami" {
#   provisioner "local-exec" {
#     command = "terraform destroy -target aws_instance.tf-instance"
#   }
# }
