terraform {
  required_version = ">= 0.13.0"
  backend "s3" {
    bucket  = "bucket-for-tfstate"
    key     = "terraform.tfstate"
    region  = "ap-northeast-1"
    encrypt = true
  }
}

provider "aws" {
  region = var.region
}
