resource "aws_s3_bucket" "bucket-for-tfstate" {
  bucket = "bucket-for-tfstate"
  versioning {
    enabled = true
  }
  lifecycle {
    prevent_destroy = true
  }
}
