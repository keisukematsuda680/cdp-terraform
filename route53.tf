resource "aws_route53_zone" "cdp-zone" {
  name = "clouddesignpattern.club"
  #   name_servers = [
  #     "ns-1480.awsdns-57.org",
  #     "ns-1699.awsdns-20.co.uk",
  #     "ns-318.awsdns-39.com",
  #     "ns-964.awsdns-56.net",
  #   ]
  tags = {
    "Name" = "clouddesignpattern.club"
  }
}

resource "aws_route53_record" "name" {
  zone_id = aws_route53_zone.cdp-zone.zone_id
  name    = "www"
  type    = "CNAME"
  ttl     = 30
  records = ["${aws_lb.tf-alb.dns_name}"]
}
