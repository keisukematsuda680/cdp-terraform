# Launch Configuration
resource "aws_launch_configuration" "terraform" {
  image_id        = aws_ami_from_instance.tf-ami-from-ec2.id
  instance_type   = "t2.micro"
  security_groups = ["${module.security_group_ec2.this_security_group_id}"]

  lifecycle {
    create_before_destroy = true
  }
}

resource "aws_autoscaling_group" "terraform" {
  launch_configuration = aws_launch_configuration.terraform.id
  target_group_arns    = ["${aws_lb_target_group.tf-alb-target-group.arn}"]
  vpc_zone_identifier  = ["${aws_subnet.public-a.id}", "${aws_subnet.public-c.id}"]

  health_check_type = "ELB"


  min_size = 1
  max_size = 3
  tag {
    key                 = "Name"
    value               = "terraform-autoscaling"
    propagate_at_launch = true
  }
}

# Scaling Policy
resource "aws_autoscaling_policy" "terraform-scale-out" {
  name                   = "terraform-scale-out"
  scaling_adjustment     = 1
  adjustment_type        = "ChangeInCapacity"
  cooldown               = 300
  autoscaling_group_name = aws_autoscaling_group.terraform.name
}

resource "aws_autoscaling_policy" "terraform-scale-in" {
  name                   = "terraform-scale-in"
  scaling_adjustment     = -1
  adjustment_type        = "ChangeInCapacity"
  cooldown               = 300
  autoscaling_group_name = aws_autoscaling_group.terraform.name
}

# Alarm
resource "aws_cloudwatch_metric_alarm" "terraform-cpu-high" {
  alarm_name          = "terraform-cpu-high"
  comparison_operator = "GreaterThanOrEqualToThreshold"
  evaluation_periods  = "1"
  metric_name         = "CPUUtilization"
  namespace           = "AWS/EC2"
  period              = "300"
  statistic           = "Average"
  threshold           = "70"
  dimensions = {
    AutoScalingGroupName = aws_autoscaling_group.terraform.name
  }
  alarm_actions = ["${aws_autoscaling_policy.terraform-scale-out.arn}"]
}

resource "aws_cloudwatch_metric_alarm" "terraform-cpu-low" {
  alarm_name          = "terraform-cpu-low"
  comparison_operator = "LessThanThreshold"
  evaluation_periods  = "1"
  metric_name         = "CPUUtilization"
  namespace           = "AWS/EC2"
  period              = "300"
  statistic           = "Average"
  threshold           = "5"
  dimensions = {
    AutoScalingGroupName = aws_autoscaling_group.terraform.name
  }
  alarm_actions = ["${aws_autoscaling_policy.terraform-scale-in.arn}"]
}
