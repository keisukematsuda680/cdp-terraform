variable "name" {
  default = "terraform"
}

variable "region" {
  default = "ap-northeast-1"
}

variable "vpc_cidr" {
  default = "172.16.0.0/16"
}

variable "instance_types" {
  default = "t2.micro"
}

variable "public_key_path" {
  default = "/Users/keisuke/.ssh/id_rsa.pub"
}

variable "wordpress_instance_ami" {
  default = "ami-0138ef0d838de5741"
}
